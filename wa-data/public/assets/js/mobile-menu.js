(function ($) {
    $.LOG_ENABLE = false;

    var leftMenu, mobileMenu, mobileNavToggler, mobileNavOverlay, bodyEl, mobileNav, header;

    var MOBILE_NAV_OPEN_CLASS = 'mobile-nav--open';
    var LEFTMENU_MOBILE_CLASS = 'leftmenu--mobile';
    var HEADER_FIXED_CLASS = 'header--fixed';
    var TOGGLER_ACTIVE_CLASS = 'mobile-nav-toggle--active';
    var MOBILE_WIDTH = 1024;
    var MOBILE_W_FIXED_HEADER_WIDTH = 756;

    $.addEventListener('resize', handleResizeWindow);

    function handleResizeWindow() {
        init();
    }

    function init() {
        if ($.innerWidth <= MOBILE_WIDTH) {
            initMobileMenu();
        } else {
            deInitMobileMenu();
        }

        if ($.innerWidth < MOBILE_W_FIXED_HEADER_WIDTH) {
            $.addEventListener('scroll', addFixedHeader); 
            addFixedHeader();
        } else {
            $.removeEventListener('scroll', addFixedHeader);
        }
    }

    function addFixedHeader() {
        var scrollTop = $.pageYOffset || document.documentElement.scrollTop;
        header.classList.toggle(HEADER_FIXED_CLASS, scrollTop >= 94);
    }

    function initMobileMenu() {
        log('initMobileMenu');
        initElements();
        if (mobileNavToggler) {
            mobileNavToggler.addEventListener('click', handleClickMenuToggle);
        }
        document.addEventListener('click', handleClickDocument);
    }

    function deInitMobileMenu() {
        if (mobileNavToggler) {
            mobileNavToggler.removeEventListener('click', handleClickMenuToggle);
        }
        document.removeEventListener('click', handleClickDocument);

        if (leftMenu && mobileMenuExists()) {
            mobileNav.removeChild(mobileMenu);
        }

        toggleElements(false);
    }

    function initElements() {
        log('initElements');
        leftMenu = document.querySelectorAll('.lmenu');
        mobileNav = document.querySelector('.mobile-nav');
        mobileNavToggler = document.querySelector('#mobile-nav-toggle');
        mobileNavOverlay = document.querySelector('.mobile-nav-overlay');
        header = document.querySelector('.header');
        bodyEl = document.querySelector('.bd-bg');
    }

    function handleClickMenuToggle(e) {
        e.preventDefault();
        e.stopPropagation();
        log('handleClickMenuToggle START');

        var openMenu = !menuIsOpen();
        cloneMobileMenu();
        toggleElements(openMenu);
        setMenuMaxHeightIfNeeded();
    }

    function toggleElements(active) {
        var isActive = active || false;
        log('toggleClassNames', isActive);

        toggleMobileNav(isActive);
        toggleMobileNavToggler(isActive);

        if ($.innerWidth < 800 || !isActive) {
            toggleOverlay(isActive);
            toogleBodyOverflow(isActive);
        }
    }

    function toggleMobileNav(active) {
        if (!mobileNav) return;

        var isActive = active || false;
        mobileNav.classList.toggle(MOBILE_NAV_OPEN_CLASS, isActive);
    }

    function toggleMobileNavToggler(active) {
        if (!mobileNavToggler) return;

        var isActive = active || false;
        mobileNavToggler.classList.toggle(TOGGLER_ACTIVE_CLASS, isActive);
    }

    function cloneMobileMenu() {
        if (!mobileMenuExists()) {
            mobileMenu = document.querySelector('.' + LEFTMENU_MOBILE_CLASS);
            var menuWrapper = document.createElement('div');
            menuWrapper.classList.add(LEFTMENU_MOBILE_CLASS);
            leftMenu.forEach(function (menu) {
                var menuClone = menu.cloneNode(true);
                menuWrapper.appendChild(menuClone);
            });
            mobileNav.appendChild(menuWrapper);
        }
    }

    function setMenuMaxHeightIfNeeded() {
        if (!mobileNav) return;

        if (menuIsOpen()) {
            setMenuMaxHeight();
        } else {
            log('setMenuMaxHeightIfNeeded  = 0');
            mobileNav.style.maxHeight = 0;
        }
    }

    function setMenuMaxHeight() {
        log('setMenuMaxHeight');
        var FIXED_HEADER_HEIGHT = 35;
        var NORMAL_HEADER_HEIGHT = 127;

        if (!header) return;

        function setInnerHeight(height) {
            if (!mobileNav) return;

            var heightCSS = $.innerHeight - height + 'px';

            log('setInnerHeight', heightCSS);
            mobileNav.style.maxHeight = heightCSS;
        }

        if (header.classList.contains(HEADER_FIXED_CLASS)) {
            setInnerHeight(FIXED_HEADER_HEIGHT);
        } else {
            setInnerHeight(NORMAL_HEADER_HEIGHT);
        }
    }
    
    function handleClickDocument() {
        log('handleClickDocument');

        if (mobileNav) {
            mobileNav.classList.remove(MOBILE_NAV_OPEN_CLASS);
        }
        if (mobileNavToggler) {
            mobileNavToggler.classList.remove(TOGGLER_ACTIVE_CLASS);
        }
        toogleBodyOverflow(false);
        toggleOverlay(false);
    }

    function menuIsOpen() {
        return mobileNav ? mobileNav.classList.contains(MOBILE_NAV_OPEN_CLASS) : false;
    }

    function mobileMenuExists() {
        return Boolean(document.querySelector('.' + LEFTMENU_MOBILE_CLASS));
    }
    
    function toggleOverlay(isOpen) {
        var overlayWillOpen = isOpen || false;

        if (mobileNavOverlay) {
            log('toggleOverlay', overlayWillOpen);
            mobileNavOverlay.style.display = overlayWillOpen ? 'block' : null;
        }
    }

    function toogleBodyOverflow(isHidden) {
        var overflowHidden = isHidden || false;

        if (bodyEl) {
            log('toogleBodyOverflow', overflowHidden);
            bodyEl.style.overflowY = overflowHidden ? 'hidden' : null;
        }
    }

    function log(messages, param) {
        if ($.LOG_ENABLE) {
            console.log(messages, param);
        }
    }

    $.initMobileMenu = init;
})(window);