!function ($) {
    /* test */
    function setMenuMaxHeight(e) {
        $header.hasClass("header--fixed") ? e.css("max-height", window.innerHeight - 35 + "px") : e.css("max-height", window.innerHeight - 127 + "px")
    }

    function t() {
        var $categoryProducts = $(".category__products"), $categorySubmenu = $(".category__submenu-wrap");
        $(".category-submenu--mobile").length || $categoryProducts.append($categorySubmenu.clone().addClass("category-submenu--mobile"))
    }

    function o() {
        window.innerWidth < 800 && t()
    }

    var $header = $(".header");
    $(document).ready(function () {
        function handleFixHeader() {
            if (window.innerWidth < 756) {
                var scrollTop = $(window).scrollTop();
                scrollTop >= 94 ? $header.addClass("header--fixed") : $header.removeClass("header--fixed")
            }
        }

        handleFixHeader(), $(window).scroll(function () {
            handleFixHeader()
        }), $(".tab__link").on("click", function (n) {
            n.preventDefault(), n.stopPropagation(), $(".tab__link").not(this).removeClass("active"), $(this).toggleClass("active"), $("body").on("click", function () {
                $(".tab__link").removeClass("active")
            })
        });
        var $leftMenu = $(".lmenu"),
            $mobileNav = $(".mobile-nav");
        $("#mobile-nav-toggle").click(function (t) {
            t.preventDefault(), t.stopPropagation(), $mobileNav.toggleClass("mobile-nav--open"), $mobileNav.hasClass("mobile-nav--open") ? setMenuMaxHeight($mobileNav) : $mobileNav.css("max-height", 0), $(".leftmenu--mobile").length || $mobileNav.prepend($leftMenu.clone().addClass("leftmenu--mobile"))
        }), $(window).resize(function () {
            window.innerWidth > 1024 && $(".leftmenu--mobile").length && ($mobileNav.removeClass("leftmenu--mobile"), $(".leftmenu--mobile").remove()), window.innerWidth < 800 && !$(".category-submenu--mobile").length && t(), handleFixHeader(), $mobileNav.hasClass("mobile-nav--open") && setMenuMaxHeight($mobileNav)
        }), $(document).on("click", function () {
            $(".mobile-nav--open").removeClass("mobile-nav--open")
        });
        var r = document.forms.search;
        $(r).on("submit", function (e) {
            e.preventDefault(), r.elements.query.value.length < 3 || r.submit()
        }), o()
    })
}(jQuery), function (e) {
    function n() {
        var n = document.querySelectorAll(".lmenu .selected");
        n.length && e(".selected").parents(".parent").each(function () {
            e(this).children(".lmenu__link").addClass("lmenu__link--parent")
        })
    }

    function t() {
        var e = r();
        E = e.top, H = e.bottom
    }

    function o() {
        L = r().left
    }

    function i() {
        z.forEach(function (e) {
            e.classList.add(j)
        })
    }

    function l() {
        A || (A = document.querySelectorAll("." + P))
    }

    function c() {
        A || z.forEach(function (e) {
            var n = document.createElement("div");
            n.className = P, e.parentNode.insertBefore(n, e), n.appendChild(e)
        })
    }

    function u() {
        return window.innerHeight
    }

    function r() {
        return N.getBoundingClientRect()
    }

    function s(e) {
        return e ? e.getBoundingClientRect() : document.querySelector("." + F).getBoundingClientRect()
    }

    function a(e) {
        var n, t = e || window.event;
        t && "keydown" == t.type && (n = t.keyCode || t.which), !A || n !== Q && void 0 !== n || A.forEach(function (e) {
            e.classList.remove(D)
        })
    }

    function m() {
        if (A && A.length) {
            var e = U < u() ? U + "px" : "100%";
            q = E > 0 ? E : 0, S = L + G, A[0].style.height = e, A[0].style.top = q + "px", W = A[0].getBoundingClientRect().bottom, A.forEach(function (n) {
                n.style.height = e, n.style.maxHeight = e, n.style.left = S + "px", W >= u() ? (n.style.bottom = "0px", n.style.top = "") : n.style.top = q + "px"
            })
        }
    }

    function d() {
        var e = document.querySelectorAll(".menu-gutter");
        e.length && e.forEach(function (e) {
            e.parentNode.removeChild(e)
        })
    }

    function f(e, n) {
        n.classList.remove("subcategory--1col"), n.classList.remove("subcategory--2col"), n.classList.remove("subcategory--3col"), n.classList.remove("subcategory--4col"), n.classList.remove("subcategory--5col"), n.style.columnCount = e, n.style.MozColumnCount = e, n.style.webkitColumnCount = e, n.classList.add("subcategory--" + e + "col")
    }

    function h(e, n) {
        if (n > 1) for (var t = 2; t <= n; t++) {
            var o = document.createElement("div");
            o.className = "menu-gutter menu-gutter--" + t, e.parentNode.appendChild(o)
        }
    }

    function v(e, n) {
        var t, o = 1;
        if (e) {
            var i = e.offsetHeight;
            if (t = n.offsetHeight, t > i && o < 6) {
                for (d(), o; o < 6 && (f(o, n), t = n.offsetHeight, !(t < i)); o++) ;
                h(o)
            }
        }
    }

    function g(e, n) {
        var t, o, i, l = s(e), c = n.firstChild;
        c && (c.style.top = "", i = c.offsetHeight, i + 2 * O > n.offsetHeight && (v(n, c), i = c.offsetHeight), t = l.top + K - O - q - i / 2, o = U - i - 2 * O, t < 0 && q > 0 ? c.style.top = "-12px" : t < 0 ? c.style.top = "0px" : t > 0 && i + t + 2 * O < U ? c.style.top = t + "px" : i + t + 2 * O > U && (W >= window.innerHeight ? c.style.top = o + "px" : c.style.top = o + O + "px"))
    }

    function p() {
        R && R.forEach(function (e) {
            e.classList.add(I)
        })
    }

    function y() {
        R.length > 0 && R.forEach(function (e) {
            e.classList.remove(J)
        })
    }

    function b(e) {
        var n, t, o = e.target;
        o.classList.contains(I) && (n = o, t = n.nextElementSibling, y(), a(), t && (g(n, t), t.classList.add(D)), B = n)
    }

    function w() {
        o(), t(), p(), i(), c(), l(), m(), T = !0
    }

    function C() {
        x(V, "mouseover", a), x(X, "mouseover", a), x(M, "keydown", a), x(M, "click", a), x(N, "mouseover", b), T = !0
    }

    function _() {
        k(V, "mouseover", a), k(X, "mouseover", a), k(M, "keydown", a), k(M, "click", a), k(N, "mouseover", b), e("." + P).css({
            left: "",
            height: ""
        }), T = !1
    }

    function x(e, n, t) {
        e.attachEvent ? e.attachEvent("on" + n, t) : e.addEventListener(n, t)
    }

    function k(e, n, t) {
        e.detachEvent ? e.detachEvent("on" + n, t) : e.removeEventListener(n, t)
    }

    var E, L, H, q, S, W, A, B, N = document.querySelector(".lmenu"),
        R = document.querySelectorAll(".lmenu:not(.leftmenu--mobile) > li >.lmenu__link"),
        z = document.querySelectorAll(".lmenu:not(.leftmenu--mobile) > .parent > .menu-v"), D = "open",
        j = "subcategory", P = "subcategory-wrap", Q = 27, M = document.body, T = !1, F = "aside", G = 240,
        I = "lmenu__parent-link", J = "lmenu__link--active", K = 17, O = 12,
        U = document.querySelector(".lmenu").clientHeight, V = document.querySelector(".header"),
        X = document.querySelector(".mainarea__content");
    e(window).resize(function () {
        window.innerWidth > 1024 ? (T || (w(), C()), o(), m()) : _()
    }), window.onscroll = function () {
        window.innerWidth > 1024 && (t(), m())
    }, e(document).ready(function () {
        window.innerWidth > 1024 && (w(), C()), n()
    })
}(jQuery);