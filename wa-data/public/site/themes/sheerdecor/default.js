// Show Menu on Hover
( function($) {

    var enter, leave;

    var storage = {
        activeClass: "submenu-is-shown",
        activeShadowClass: "is-shadow-shown",
        showTime: 200,
        $last_li: false
    };

    var bindEvents = function() {
        var $selector = $(".flyout-nav > li"),
            links = $selector.find("> a");

        $selector.on("mouseenter", function() {
            showSubMenu( $(this) );
        });

        $selector.on("mouseleave", function() {
            hideSubMenu( $(this) );
        });

        links.on("click", function() {
            onClick( $(this).closest("li") );
        });

        links.each( function() {
            var link = this,
                $li = $(link).closest("li"),
                has_sub_menu = ( $li.find(".flyout").length );

            if (has_sub_menu) {
                link.addEventListener("touchstart", function(event) {
                    onTouchStart(event, $li );
                }, false);
            }
        });

        $("body").get(0).addEventListener("touchstart", function(event) {
            onBodyClick(event, $(this));
        }, false);

    };

    var onBodyClick = function(event) {
        var activeBodyClass = storage.activeShadowClass,
            is_click_on_shadow = ( $(event.target).hasClass(activeBodyClass) );

        if (is_click_on_shadow) {
            var $active_li = $(".flyout-nav > li." + storage.activeClass).first();

            if ($active_li.length) {
                hideSubMenu( $active_li );
            }
        }
    };

    var onClick = function( $li ) {
        var is_active = $li.hasClass(storage.activeClass);

        if (is_active) {
            var href = $li.find("> a").attr("href");
            if ( href && (href !== "javascript:void(0);") ) {
                hideSubMenu( $li );
            }

        } else {
            showSubMenu( $li );
        }
    };

    var onTouchStart = function(event, $li) {
        event.preventDefault();

        var is_active = $li.hasClass(storage.activeClass);

        if (is_active) {
            hideSubMenu( $li );
        } else {
            var $last_li = $(".flyout-nav > li." +storage.activeClass);
            if ($last_li.length) {
                storage.$last_li = $last_li;
            }
            showSubMenu( $li );
        }
    };

    var showSubMenu = function( $li ) {
        var is_active = $li.hasClass(storage.activeClass),
            has_sub_menu = ( $li.find(".flyout").length );

        if (is_active) {
            clearTimeout( leave );

        } else {
            if (has_sub_menu) {

                enter = setTimeout( function() {

                    if (storage.$last_li && storage.$last_li.length) {
                        clearTimeout( leave );
                        storage.$last_li.removeClass(storage.activeClass);
                    }

                    $li.addClass(storage.activeClass);
                    toggleMainOrnament(true);
                }, storage.showTime);
            }
        }
    };

    var hideSubMenu = function( $li ) {
        var is_active = $li.hasClass(storage.activeClass);

        if (!is_active) {
            clearTimeout( enter );

        } else {
            storage.$last_li = $li;

            leave = setTimeout(function () {
                $li.removeClass(storage.activeClass);
                toggleMainOrnament(false);
            }, storage.showTime * 2);
        }
    };

    var toggleMainOrnament = function($toggle) {
        var $body = $("body"),
            activeClass = storage.activeShadowClass;

        if ($toggle) {
            $body.addClass(activeClass);
        } else {
            $body.removeClass(activeClass);
        }
    };

    $(document).ready( function() {
        bindEvents();
    });

})(jQuery);

var MatchMedia = function( media_query ) {
    var matchMedia = window.matchMedia,
        is_supported = (typeof matchMedia === "function");
    if (is_supported && media_query) {
        return matchMedia(media_query).matches
    } else {
        return false;
    }
};

$(document).ready(function() {

    // custom LOGO position adjustment
    if ($('#logo').length)
    {
        $(window).load(function(){
        
            var _logo_height = $('#logo').height();
            var _logo_vertical_shift = Math.round((_logo_height-25)/2);
            
            $('#globalnav').css('padding-top', _logo_vertical_shift+'px');
            $('#logo').css('margin-top', '-'+_logo_vertical_shift+'px');
        
        });
    }

    // MOBILE nav slide-out menu
    /*$('#mobile-nav-toggle').click( function(){
     if (!$('.nav-negative').length) {
     $('body').prepend($('header .apps').clone().removeClass('apps').addClass('nav-negative'));
     $('body').prepend($('header .auth').clone().addClass('nav-negative'));
     $('body').prepend($('header .search').clone().addClass('nav-negative'));
     $('body').prepend($('header .offline').clone().addClass('nav-negative'));
     $('.nav-negative').hide().slideToggle(200);
     } else {
     $('.nav-negative').slideToggle(200);
     }
     $("html, body").animate({ scrollTop: 0 }, 200);
     return false;
     });*/
        
    // MAILER app email subscribe form
    $('#mailer-subscribe-form input.charset').val(document.charset || document.characterSet);
    $('#mailer-subscribe-form').submit(function() {
        var form = $(this);

        var email_input = form.find('input[name="email"]');
        var email_submit = form.find('input[type="submit"]');
        if (!email_input.val()) {
            email_input.addClass('error');
            return false;
        } else {
            email_input.removeClass('error');
        }
        
        email_submit.hide();
        email_input.after('<i class="icon16 loading"></i>');

        $('#mailer-subscribe-iframe').load(function() {
            $('#mailer-subscribe-form').hide();
            $('#mailer-subscribe-iframe').hide();
            $('#mailer-subscribe-thankyou').show();
        });
    });
    
    // STICKY CART for non-mobile
    if ( !(MatchMedia("only screen and (max-width: 760px)")) ) {
        $(window).scroll(function(){
           	if ( $(this).scrollTop() >= 55 && !$("#cart").hasClass( "fixed" ) && !$("#cart").hasClass( "empty" ) && !($(".cart-summary-page")).length ) {
           	    //$("#cart").hide();
           	    
           	    //$("#cart").addClass( "fixed" );
           	    if ($('#cart-flyer').length)
           	    {
           	        var _width = $('#cart-flyer').width()+52;
           	        var _offset_right = $(window).width() - $('#cart-flyer').offset().left - _width + 1;
           	        
           	       // $("#cart").css({ "right": _offset_right+"px", "width": _width+"px" });
           	    }
           	    
           	    //$("#cart").slideToggle(200);
           	} else if ( $(this).scrollTop() < 50 && $("#cart").hasClass( "fixed" ) ) {
    	        //$("#cart").removeClass( "fixed" );
        	    //$("#cart").css({ "width": "auto" });
        	}
        });
    }
});

$(document).ready(function(){

	
	if($("#home_list").width() > 756) {
		$("#home_list_text1").insertAfter($("#temp_id5"));
		$("#home_list_text2").insertAfter($("#temp_id10"));
		$("#home_list_text3").insertAfter($("#temp_id15"));
		$("#home_list_text4").insertAfter($("#temp_id20"));
	} else {
		$("#home_list").append("<div class='extra_blocks_wrap'><a class='btn extrablock_trigger home_et'>Показать описание</a><div class='extra_blocks'></div></div>");
		$("#home_list_text1").appendTo($(".extra_blocks"));
		$("#home_list_text2").appendTo($(".extra_blocks"));
		$("#home_list_text3").appendTo($(".extra_blocks"));
		$("#home_list_text4").appendTo($(".extra_blocks"));
	}
	
	$(document).on("click", ".home_et", function(){
		$(".category-text").css({"margin-top": "0"}).show();
		$(this).hasClass("active") ? ($(this).removeClass("active"), $(this).text("показать описание"), $(this).next(".extra_blocks").slideUp()) : ($(this).addClass("active"),  $(this).text("скрыть описание"), $(this).next(".extra_blocks").slideDown()) ;
	});
	
	function popup_window_show(popup) {
		popup.fadeIn("fast");
		$("#shadow").show();	
		return false;
	}
	
	$(document).on("click", ".not_auth", function(){
		popup_window_show($("#not_auth"));
		return false;
	});
	$(document).on("click", ".shop_favorites a.add", function(){
		popup_window_show($("#added_to_wishlist"));
	});
	$(document).on("click", ".del_popup", function(){
		popup_window_show($("#remove_from_wishlist"));
		current_product = $(this).closest(".shop_favorites").data("product-id");
		$("#remove_from_wishlist").find(".shop_favorites").attr('data-product-id', current_product); 
	});
	
	$(document).on("click", ".close_auth, #shadow, .del", function(){
		$(".popup_window").fadeOut("fast");
		$("#shadow").hide();
	});
	
	$(".wa-captcha .wa-captcha-input").attr("placeholder", "Введите код с картинки:");
	$(".wa-captcha .wa-captcha-refresh").text("Обновить код");
	
	$(".white_block").height($(".empty_td").outerHeight());
	
	// COMPARE
	function compare_popup_window(popup) {
		$(popup).fadeIn("fast");
		$("#shadow").show();	
		return false;
	}

	$('#compare-leash .row-compare').click(function () {
		in_compare = $(this).find(".compare-count").html();
		if(parseInt(in_compare) < 2) {
			compare_popup_window("#low_compare");
			return false;
		}

	});
	 // COMPARE LIST
    $(".content").on('click', '.product-list a.compare', function () {
        var compare = $.cookie('shop_compare');
		$.cookie('shop_compare', compare, { expires: 30, path: '/'});
		var data_product = $(this).data("product");
        if (!$(this).find('i').hasClass('active')) {
            if (compare) {
				if(jQuery.inArray($(this).data('product').toString(), compare.split(',')) > -1) {
					compare_popup_window("#compare_product_has");
					if (compare.split(',').length < 2) {
						$("#compare_product_has .enough_to_compare").hide();
					} else {
						$("#compare_product_has .enough_to_compare").show();
					}
					$("#compare_product_has .compare-remove").attr("data-product", data_product);
					return false;
				} else {
					if (compare.split(',').length < 9) {
						compare += ',' + $(this).data('product');
					} else {
						compare_popup_window("#compare_alert");
					}
				}
            } else {
                compare = '' + $(this).data('product');
            }
			if (compare.split(',').length > 9){
				compare_popup_window("#compare_alert");
				return false;
            } else if (compare.split(',').length > 0) {
				if (compare.split(',').length < 2) {
					compare_popup_window("#compare_one_more");
				} else {
					compare_popup_window("#compare_added");
				}
                $("#compare-leash .row-compare").toggleClass("row-link--active");
                var url = $("#compare-leash a").attr('href').replace(/compare\/.*$/, 'compare/' + compare + '/');
                $('#compare-leash a').show().find('strong').html(compare.split(',').length);
                $('#compare-leash a').show().find('.compare-count').html(compare.split(',').length);
                $('.user-row').addClass('user-row--open');
                $('.row-compare').addClass('row-link--active');
            } else {
				
            }
            $.cookie('shop_compare', compare, { expires: 30, path: '/'});
        } else {
            if (compare) {
                compare = compare.split(',');
            } else {
                compare = [];
            }
            var i = $.inArray($(this).data('product') + '', compare);
            if (i != -1) {
                compare.splice(i, 1)
            }
            if (compare.length > 0) {
                $.cookie('shop_compare', compare.join(','), { expires: 30, path: '/'});
                $('#compare-leash a').show().find('.compare-count').html(compare.length);
                $('.user-row').addClass('user-row--open');
                $('.row-compare').addClass('row-link--active');
            } else {
                $.cookie('shop_compare', null, {path: '/'});
            }
        }
        $(this).toggleClass('active');
        $(this).attr('title', "Убрать из сравнения");
        return false;
    });
	 // COMPARE PRODUCT
	 $(".product-aside").on('click', 'a.compare', function () {
        var compare = $.cookie('shop_compare');
        $.cookie('shop_compare', compare, { expires: 30, path: '/'});
        if (!$(this).find('i').hasClass('active')) {
            if (compare) {
				if(jQuery.inArray($(this).data('product').toString(), compare.split(',')) > -1) {
					compare_popup_window("#compare_product_has");
					if (compare.split(',').length < 2) {
						$("#compare_product_has .enough_to_compare").hide();
					} else {
						$("#compare_product_has .enough_to_compare").show();
					}
					return false;
				} else {
					if (compare.split(',').length < 9) {
						compare += ',' + $(this).data('product');
					} else {
						compare_popup_window("#compare_alert");
					}
				}
            } else {
                compare = '' + $(this).data('product');
            }
			if (compare.split(',').length > 9){
				compare_popup_window("#compare_alert");
				return false;
            } else if (compare.split(',').length > 0) {
				if (compare.split(',').length < 2) {
					compare_popup_window("#compare_one_more");
				} else {
					compare_popup_window("#compare_added");
				}
				$(this).addClass("selected");
				$(this).find("span").text("Убрать");
                $("#compare-leash .row-compare").toggleClass("row-link--active");
                $('#compare-leash a').show().find('strong').html(compare.split(',').length);
                $('#compare-leash a').show().find('.compare-count').html(compare.split(',').length);
                $('.user-row').addClass('user-row--open');
                $('.row-compare').addClass('row-link--active');
            }
            $.cookie('shop_compare', compare, { expires: 30, path: '/'});
        } else {
            if (compare) {
                compare = compare.split(',');
            } else {
                compare = [];
            }
            var i = $.inArray($(this).data('product') + '', compare);
            if (i != -1) {
                compare.splice(i, 1)
            }
            if (compare.length > 0) {
                $.cookie('shop_compare', compare.join(','), { expires: 30, path: '/'});
                $('#compare-leash a').show().find('.compare-count').html(compare.length);
                $('.user-row').addClass('user-row--open');
                $('.row-compare').addClass('row-link--active');
            } else {
                $.cookie('shop_compare', null, {path: '/'});
            }
			$(this).addClass('selected');
        }
        $(this).toggleClass('active');
        return false;
    });
	
	$("a.compare-remove").click(function () {
		$(this).closest(".popup_window").hide();
		$("#shadow").hide();
        var compare = $.cookie('shop_compare');
        if (compare) {
            compare = compare.split(',');
        } else {
            compare = [];
        }
        var i = $.inArray($(this).attr('data-product') + '', compare);
        if (i != -1) {
            compare.splice(i, 1)
        }
        $("#compare-link").hide();
        if (compare.length > 0) {
            $.cookie('shop_compare', compare.join(','), {expires: 30, path: '/'});
            $('#compare-leash a').show().find('strong').html(compare.length);
        } else {
            $("#compare-leash .row-compare").removeClass("row-link--active");
            $.cookie('shop_compare', null, {path: '/'});
        }
		var data_product = $(this).data("product");
		$('a.compare[data-product="'+data_product+'"]').removeClass('active');
		$('a.compare[data-product="'+data_product+'"]').attr("title", "Добавить в сравнение");
		$(".product-aside a.compare").removeClass("active");
		$(".product-aside a.compare").removeClass("selected");
		$(".product-aside a.compare span").text("Сравнить");
		$('#compare-leash a').show().find('.compare-count').html(compare.length);
        return false;
    });

});

